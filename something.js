function func1(){
    let a = 5;
    let b = 'five';
    console.log(b*a);
}

function func2(){
    for(i = func1(); i > 0; i--){
        console.log(i);
    }
}

function func3(){
    func1();
    func2();
}
